package mx.com.visorus.http_db_json.modelos;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Geocerca{
    @SerializedName("DT_RowId")
    private long DT_RowId;
    private long id;
    private String descripcion;
    private String latitud;
    private String longitud;
    private String radio;
    private List<String> wifimac;
    private boolean activo;

    public Geocerca() {
    }

    public Geocerca(long DT_RowId,long id, String descripcion, String latitud, String longitud, String radio, List<String> wifimac, boolean activo) {
        this.DT_RowId=DT_RowId;
        this.id = id;
        this.descripcion = descripcion;
        this.longitud = longitud;
        this.latitud = latitud;
        this.radio = radio;
        this.wifimac = wifimac;
        this.activo = activo;
    }

    public long getDT_RowId() {
        return DT_RowId;
    }

    public void setDT_RowId(long DT_RowId) {
        this.DT_RowId = DT_RowId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getRadio() {
        return radio;
    }

    public void setRadio(String radio) {
        this.radio = radio;
    }

    public List<String> getWifimac() {
        return wifimac;
    }

    public void setWifimac(List<String> wifimac) {
        this.wifimac = wifimac;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }
}
