package mx.com.visorus.http_db_json.sqlite;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import mx.com.visorus.http_db_json.modelos.Empleado;
import mx.com.visorus.http_db_json.modelos.Empresa;
import mx.com.visorus.http_db_json.modelos.Geocerca;
import mx.com.visorus.http_db_json.modelos.Horario;
import mx.com.visorus.http_db_json.modelos.SistemaLog;

public class CRUD_BD {
    //registros
    public static void insertarEmpresa(Empresa empresa, AyudanteSQLite ayudanteSQLite) {
        SQLiteDatabase sqLiteDatabase = ayudanteSQLite.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("id_empresa", empresa.getId());
        cv.put("clave", empresa.getClave());
        cv.put("razon_social", empresa.getRazonSocial());
        cv.put("rfc", empresa.getRfc());
        sqLiteDatabase.insert("empresa", "id_empresa", cv);
        sqLiteDatabase.close();
    }

    public static void insertarHorario(Horario horario, AyudanteSQLite ayudanteSQLite) {
        SQLiteDatabase sqLiteDatabase = ayudanteSQLite.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("DT_RowId", horario.getDT_RowId());
        cv.put("id_horario", horario.getId());
        cv.put("descripcion", horario.getDescripcion());
        cv.put("entrada", horario.getEntrada());
        cv.put("salida", horario.getSalida());
        cv.put("maxSalida", horario.getMaxSalida());
        cv.put("tipo", horario.getTipo());
        cv.put("minRetraso", horario.getMinRetraso());
        cv.put("retrasos", horario.getRetrasos());
        cv.put("premios", horario.getPremios());
        cv.put("activo", horario.isActivo());
        sqLiteDatabase.insert("horarios", "id_horaario", cv);
        sqLiteDatabase.close();
    }

    public static void insertarGeocerca(Geocerca geocerca, AyudanteSQLite ayudanteSQLite) {
        SQLiteDatabase sqLiteDatabase = ayudanteSQLite.getWritableDatabase();
        ContentValues cv = new ContentValues();
        String MACS_AP = "";
        cv.put("DT_RowId", geocerca.getDT_RowId());
        cv.put("id_geocerca", geocerca.getId());
        cv.put("descripcion", geocerca.getDescripcion());
        cv.put("latitud", geocerca.getLatitud());
        cv.put("longitud", geocerca.getLongitud());
        cv.put("radio", geocerca.getRadio());
        for (int i = 0; i < geocerca.getWifimac().size(); i++) {
            MACS_AP += geocerca.getWifimac().get(i) + ",";
        }
        cv.put("DT_RowId", MACS_AP);
        cv.put("activo", geocerca.isActivo());
        sqLiteDatabase.insert("geocercas", "id_geocerca", cv);
        sqLiteDatabase.close();
    }

    public static void insertarEmpleado(Empleado empleado, AyudanteSQLite ayudanteSQLite) {
        SQLiteDatabase sqLiteDatabase = ayudanteSQLite.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("DT_RowId", empleado.getDT_RowId());
        cv.put("id_empleado", empleado.getId());
        cv.put("clave", empleado.getClave());
        cv.put("nombre", empleado.getNombre());
        cv.put("fk_id_empresa", empleado.getEmpresa().getId());
        cv.put("uuid", empleado.getUuid());
        cv.put("fk_id_horario", empleado.getHorario().getId());
        cv.put("status", empleado.isStatus());
        cv.put("tipo", empleado.getTipo());
        sqLiteDatabase.insert("empleados", "id_empleado", cv);
        sqLiteDatabase.close();
    }

    public static void insertarLog(Empleado empleado, AyudanteSQLite ayudanteSQLite) {
        SQLiteDatabase sqLiteDatabase = ayudanteSQLite.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("id_log", empleado.getTimeLogs().getId());
        cv.put("id_empleado", empleado.getId());
        cv.put("id_geocerca", empleado.getTimeLogs().getGeocerca().getId());
        cv.put("imagen", empleado.getTimeLogs().getImagen());
        cv.put("latitud", empleado.getTimeLogs().getLatitud());
        cv.put("longitud", empleado.getTimeLogs().getLongitud());
        cv.put("registro", empleado.getTimeLogs().getRegistro());
        cv.put("activo", empleado.getTimeLogs().isStatus());
        cv.put("tipo", empleado.getTimeLogs().getTipo());
        sqLiteDatabase.insert("sistema_logs", "id_log", cv);
        sqLiteDatabase.close();
    }

    //Actualizaciones
    public static int actualizarEmpresa(Empresa empresa, AyudanteSQLite ayudanteSQLite) {
        int actualizacion = 0;
        SQLiteDatabase sqLiteDatabase = ayudanteSQLite.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("id_empresa", empresa.getId());
        cv.put("clave", empresa.getClave());
        cv.put("razon_social", empresa.getRazonSocial());
        cv.put("rfc", empresa.getRfc());
        actualizacion = sqLiteDatabase.update("empresa", cv, "id_empresa=?", new String[]{String.valueOf(empresa.getId())});
        return actualizacion;
    }

    public static int actualizarHorario(Horario horario, AyudanteSQLite ayudanteSQLite) {
        int actualizacion = 0;
        SQLiteDatabase sqLiteDatabase = ayudanteSQLite.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("DT_RowId", horario.getDT_RowId());
        cv.put("id_horario", horario.getId());
        cv.put("descripcion", horario.getDescripcion());
        cv.put("entrada", horario.getEntrada());
        cv.put("salida", horario.getSalida());
        cv.put("maxSalida", horario.getMaxSalida());
        cv.put("tipo", horario.getTipo());
        cv.put("minRetraso", horario.getMinRetraso());
        cv.put("retrasos", horario.getRetrasos());
        cv.put("premios", horario.getPremios());
        cv.put("activo", horario.isActivo());
        actualizacion = sqLiteDatabase.update("horarios", cv, "id_horario=?", new String[]{String.valueOf(horario.getId())});
        return actualizacion;
    }

    public static int actulizarGeocerca(Geocerca geocerca, AyudanteSQLite ayudanteSQLite) {
        int actualizacion = 0;
        String MACS_AP = "";
        SQLiteDatabase sqLiteDatabase = ayudanteSQLite.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("DT_RowId", geocerca.getDT_RowId());
        cv.put("id_geocerca", geocerca.getId());
        cv.put("descripcion", geocerca.getDescripcion());
        cv.put("latitud", geocerca.getLatitud());
        cv.put("longitud", geocerca.getLongitud());
        cv.put("radio", geocerca.getRadio());
        for (int i = 0; i < geocerca.getWifimac().size(); i++) {
            MACS_AP += geocerca.getWifimac().get(i) + ",";
        }
        cv.put("DT_RowId", MACS_AP);
        cv.put("activo", geocerca.isActivo());
        actualizacion = sqLiteDatabase.update("geocercas", cv, "id_geocerca=?", new String[]{String.valueOf(geocerca.getId())});
        return actualizacion;
    }

    public static int actualizarEmpleado(Empleado empleado, AyudanteSQLite ayudanteSQLite) {
        int actualizacion = 0;
        SQLiteDatabase sqLiteDatabase = ayudanteSQLite.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("DT_RowId", empleado.getDT_RowId());
        cv.put("id_empleado", empleado.getId());
        cv.put("clave", empleado.getClave());
        cv.put("nombre", empleado.getNombre());
        cv.put("fk_id_empresa", empleado.getEmpresa().getId());
        cv.put("uuid", empleado.getUuid());
        cv.put("fk_id_horario", empleado.getHorario().getId());
        cv.put("status", empleado.isStatus());
        cv.put("tipo", empleado.getTipo());
        actualizacion = sqLiteDatabase.update("empleados", cv, "id_empleado=?", new String[]{String.valueOf(empleado.getId())});
        return actualizacion;
    }

    public static int actualizarSistema_logs(Empleado empleado, AyudanteSQLite ayudanteSQLite) {
        int actualizacion = 0;
        SQLiteDatabase sqLiteDatabase = ayudanteSQLite.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("id_log", empleado.getTimeLogs().getId());
        cv.put("id_empleado", empleado.getId());
        cv.put("id_geocerca", empleado.getTimeLogs().getGeocerca().getId());
        cv.put("imagen", empleado.getTimeLogs().getImagen());
        cv.put("latitud", empleado.getTimeLogs().getLatitud());
        cv.put("longitud", empleado.getTimeLogs().getLongitud());
        cv.put("registro", empleado.getTimeLogs().getRegistro());
        cv.put("activo", empleado.getTimeLogs().isStatus());
        cv.put("tipo", empleado.getTimeLogs().getTipo());
        actualizacion = sqLiteDatabase.update("sistema_logs", cv, "id_empleado=?", new String[]{String.valueOf(empleado.getId())});
        return actualizacion;
    }

    //eliminacion
    public static void eliminarLog(SistemaLog logs, AyudanteSQLite ayudanteSQLite) {
        SQLiteDatabase sqLiteDatabase = ayudanteSQLite.getWritableDatabase();
        sqLiteDatabase.delete("sistema_logs", "id_empleado=?", new String[]{String.valueOf(logs.getId_empleado())});
        sqLiteDatabase.close();
    }

    //consultas
    public static Empresa obtenerEmpresaDeEmpleado(String id_empresa, AyudanteSQLite ayudanteSQLite) {
        Empresa empresa = new Empresa();
        SQLiteDatabase sqLiteDatabase = ayudanteSQLite.getReadableDatabase();
        String[] columnas = new String[]{"id_empresa", "clave", "razon_social", "rfc"};
        Cursor c = sqLiteDatabase.query("empresa", columnas, "id_empresa=?", new String[]{id_empresa}, null, null, null);
        if (c.moveToFirst()) {
            empresa.setId((long) c.getInt(0));
            empresa.setClave(c.getString(1));
            empresa.setRazonSocial(c.getString(2));
            empresa.setRfc(c.getString(3));
        } else {
            ///indica un error a nivel de usuario comun
        }

        return empresa;
    }

    public static JSONObject obtenerLog(AyudanteSQLite ayudanteSQLite){
        JSONObject jsonObject=new JSONObject();

        SQLiteDatabase sqLiteDatabase=ayudanteSQLite.getReadableDatabase();
        //String[] columnas=new String[]{"id_log","id_empleado","id_geocerca","imagen","latitud","longitud","registro","activo","tipo"};
       // Cursor c=sqLiteDatabase.query("sistema_logs",columnas,null,null,null,null,null);
       // c.moveToFirst();
        try {
            jsonObject.put("id",2);
            jsonObject.put("version",0);
            jsonObject.put("uuid","cf5433728aad791a");
            jsonObject.put("registro",848333);
            jsonObject.put("tipo",1);
            jsonObject.put("geocerca_id",23);
            jsonObject.put("imagen", null);
            jsonObject.put("latitud", 0);
            jsonObject.put("longitud",0);
            jsonObject.put("status",0);
            return jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static long obtenerID(String tabla, String columnaClave, String valorColumanaClave, AyudanteSQLite ayudanteSQLite) {
        long id=0;
        SQLiteDatabase sqLiteDatabase = ayudanteSQLite.getReadableDatabase();
        Cursor c = sqLiteDatabase.query(tabla, new String[]{columnaClave}, columnaClave + "=?", new String[]{valorColumanaClave},
        null, null, null);
        if(c.moveToFirst()){
            id=(long)c.getColumnIndex(columnaClave);
        }else{
            //comunicar a nivel de usuario comun
        }
        return id;
    }
}
