package mx.com.visorus.http_db_json.tareas_de_red.serviciosAPI;

import java.util.List;

import mx.com.visorus.http_db_json.modelos.Geocerca;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GeocercaServicio {
    @GET("/vNomina/geocerca/index")
    Call<List<Geocerca>> obtener_geocercas_empresa(@Query("empresa") long id_empresa);
}
