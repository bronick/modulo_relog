package mx.com.visorus.http_db_json;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import mx.com.visorus.http_db_json.modelos.Empleado;
import mx.com.visorus.http_db_json.modelos.SistemaLog;
import mx.com.visorus.http_db_json.sqlite.AyudanteSQLite;
import mx.com.visorus.http_db_json.sqlite.CRUD_BD;
import mx.com.visorus.http_db_json.tareas_de_red.API;
import mx.com.visorus.http_db_json.tareas_de_red.serviciosAPI.EmpleadoServicio;
import mx.com.visorus.http_db_json.tareas_de_red.serviciosAPI.LogServicio;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CRUD_Logs extends AppCompatActivity implements View.OnClickListener{
    static final String TAG = "CRUD_Logs";
    private Button btnEnviar,btnGuardar,btnEliminar,btnConsultar;
    private TextView tvMostrarLog;
    private AyudanteSQLite ayudanteSQLite;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crud__logs);
        ayudanteSQLite=new AyudanteSQLite(this);
        btnEnviar=(Button)findViewById(R.id.btnEnviarLogNomnia);
        btnGuardar=(Button)findViewById(R.id.btnGuardarLog);
        btnEliminar=(Button)findViewById(R.id.btnEliminarLog);
        btnConsultar=(Button)findViewById(R.id.btnObtenerLog);
        tvMostrarLog=(TextView)findViewById(R.id.tvResultLogs);

        btnEnviar.setOnClickListener(this);
        btnGuardar.setOnClickListener(this);
        btnConsultar.setOnClickListener(this);
        btnEliminar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId()==btnEnviar.getId()){
           enviarLogNomina();
        } else if(view.getId()==btnGuardar.getId()){

        } else if(view.getId()==btnEliminar.getId()){

        } else if(view.getId()==btnConsultar.getId()){

        }
    }
    public class EnviarLogNomina extends AsyncTask<String, Integer,String>{
        @Override
        protected String doInBackground(String... params) {
         enviarLogNomina();
            return null;
        }
    }

    private void realizarTareaObtenerEmpleado(){
        EmpleadoServicio empleadoServicio= API.obtenerApi().create(EmpleadoServicio.class);
        Call<Empleado> empleadoCall=empleadoServicio.obtener_empleado("cf5433728aad791a");
        empleadoCall.enqueue(new Callback<Empleado>() {
            @Override
            public void onResponse(Call<Empleado> call, Response<Empleado> response) {
                Empleado empleado=response.body();
            }

            @Override
            public void onFailure(Call<Empleado> call, Throwable t) {
                Toast.makeText(null,"No se pudo recuperar el empleado",Toast.LENGTH_LONG).show();
            }
        });
    }
    private void enviarLogNomina(){
        JSONObject PostData;
        PostData=CRUD_BD.obtenerLog(ayudanteSQLite);
        SistemaLog sistemaLog=new Gson().fromJson(PostData.toString(), SistemaLog.class);
        LogServicio logServicio=API.obtenerApi().create(LogServicio.class);
        Call<SistemaLog> logServicioCall=logServicio.guardar_log(sistemaLog);
        Log.d("CRUD_Logs","___________________________"+PostData.toString());
        logServicioCall.enqueue(new Callback<SistemaLog>() {
            @Override
            public void onResponse(Call<SistemaLog> call, Response<SistemaLog> response) {
                if(!response.isSuccessful())
                    return;

                Log.i(TAG, "onResponse: status: " +response.headers());
                Log.i(TAG, "onResponse: isSuccessful: " +response.isSuccessful());
                Log.i(TAG, "onResponse: isSuccessful: " +response.errorBody());
                Log.d("CRUD_Logs", "onResponse: "+response.body());
                Log.d("CRUD_Logs", "message: "+response.message());
            }

            @Override
            public void onFailure(Call<SistemaLog> call, Throwable t) {
                Log.i("CRUD_Logs", "onFailure: " + call.toString());
            }
        });

        //EnviarLogNomina enviarLogNomina=new EnviarLogNomina();
        //enviarLogNomina.execute("http://192.168.100.105:8070/vNomina/timeLog/save", PostData.toString());
    }



}
