package mx.com.visorus.http_db_json.modelos;

import com.google.gson.annotations.SerializedName;

public class Empresa{
    private long id;
    private String clave;
    @SerializedName("razonSocial")
    private String razonSocial;
    private String rfc;

    public Empresa() {
    }

    public Empresa(long id, String clave, String razon_social, String rfc) {
        this.id = id;
        this.clave = clave;
        this.razonSocial = razon_social;
        this.rfc=rfc;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public long getId() {
        return id;
    }

    public void setId(long id_e) {
        this.id = id_e;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }
}
