package mx.com.visorus.http_db_json.modelos;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

public class Empleado{
    private long DT_RowId;
    private long id;
    private String clave;
    private String nombre;
    private Empresa empresa;
    private String uuid;
    private Horario horario;
    private SistemaLog timeLogs;
    private boolean status;
    private int tipo;

    public Empleado() {

    }

    public Empleado(long id, long DT_RowId,String clave, String uuid, String nombre, Empresa empresa, boolean status, Horario horario, int tipo, SistemaLog timeLogs) {
        this.id = id;
        this.DT_RowId=DT_RowId;
        this.clave=clave;
        this.uuid = uuid;
        this.nombre = nombre;
        this.empresa = empresa;
        this.status = status;
        this.horario = horario;
        this.timeLogs=timeLogs;
        this.tipo=tipo;
    }


    public SistemaLog getTimeLogs() {
        return timeLogs;
    }

    public void setTimeLogs(SistemaLog timeLogs) {
        this.timeLogs = timeLogs;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public long getDT_RowId() {
        return DT_RowId;
    }

    public void setDT_RowId(long DT_RowId) {
        this.DT_RowId = DT_RowId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Horario getHorario() {
        return horario;
    }

    public void setHorario(Horario horario) {
        this.horario = horario;
    }

    public static Empresa parseJSON(String response){
        Gson gson=new GsonBuilder().create();
        Empresa empresa=gson.fromJson(response,Empresa.class);
        return empresa;
    }
    public static Horario parseJSONHorario(String response){
        Gson gson=new GsonBuilder().create();
        Horario horario=gson.fromJson(response,Horario.class);
        return horario;
    }
    public static SistemaLog parseJSONLog(String response){
        Gson gson=new GsonBuilder().create();
        SistemaLog timeLogs=gson.fromJson(response,SistemaLog.class);
        return timeLogs;
    }
}
