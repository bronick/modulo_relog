package mx.com.visorus.http_db_json.tareas_de_red.serviciosAPI;

import org.json.JSONArray;
import org.json.JSONObject;

import mx.com.visorus.http_db_json.modelos.SistemaLog;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by pc on 12/07/2018.
 */

public interface LogServicio {
    @POST("vNomina/timeLog/save")
    Call<SistemaLog> guardar_log(@Body SistemaLog log);

    @POST("vNomina/timeLog/save")
    Call<SistemaLog> guardar_log(@Body JSONArray log);
}
