package mx.com.visorus.http_db_json;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

import mx.com.visorus.http_db_json.modelos.Empleado;
import mx.com.visorus.http_db_json.modelos.Geocerca;
import mx.com.visorus.http_db_json.sqlite.AyudanteSQLite;
import mx.com.visorus.http_db_json.sqlite.CRUD_BD;
import mx.com.visorus.http_db_json.tareas_de_red.API;
import mx.com.visorus.http_db_json.tareas_de_red.serviciosAPI.EmpleadoServicio;
import mx.com.visorus.http_db_json.tareas_de_red.serviciosAPI.GeocercaServicio;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private static long id_empresa=0;
    private static Empleado empleado;
    private static List<String> macs;
    private static List<Geocerca> geocercas;
    private TextView tvM;
    private Button btnE;
    private AyudanteSQLite ayudanteSQLite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ayudanteSQLite=new AyudanteSQLite(this);
        tvM=(TextView)findViewById(R.id.tvM);
        btnE=(Button)findViewById(R.id.btnEntrar);
        btnE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        Tarea tarea=new Tarea();
        tarea.execute();
    }

    private void realizarTareaObtenerEmpleadoSincrono(){
        EmpleadoServicio empleadoServicio= API.obtenerApi().create(EmpleadoServicio.class);
        Call<Empleado> empleadoCall=empleadoServicio.obtener_empleado("cf5433728aad791a");
        try {
            empleado=empleadoCall.execute().body();
            id_empresa=empleado.getEmpresa().getId();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void realizarTareaObtenerGeocercaSincrono(){
        GeocercaServicio geocercaServicio=API.obtenerApi().create(GeocercaServicio.class);
        Call<List<Geocerca>> geocercaCall=geocercaServicio.obtener_geocercas_empresa(id_empresa);
        try {
            geocercas=geocercaCall.execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public class Tarea extends AsyncTask<Void, Integer, Void>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            //guardar en la base de datos
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            tvM.setText("Id geocerca:"+empleado.getTimeLogs().getGeocerca().getId());
            for(int i=0;i<geocercas.size();i++){
                if(geocercas.get(i).getId()==empleado.getTimeLogs().getGeocerca().getId()){
                    macs=geocercas.get(i).getWifimac();
                    break;
                }
            }
            tvM.setText(tvM.getText()+"\nmacS:"+macs);
            tvM.setText(tvM.getText()+"\n"+CRUD_BD.obtenerEmpresaDeEmpleado(""+empleado.getEmpresa().getId(),ayudanteSQLite).getId());
        }
    }


    private void realizarTareaObtenerEmpleado(){
        EmpleadoServicio empleadoServicio= API.obtenerApi().create(EmpleadoServicio.class);
        Call<Empleado> empleadoCall=empleadoServicio.obtener_empleado("cf5433728aad791a");
        empleadoCall.enqueue(new Callback<Empleado>() {
            @Override
            public void onResponse(Call<Empleado> call, Response<Empleado> response) {
                Empleado empleado=response.body();
                id_empresa=empleado.getEmpresa().getId();
                tvM.setText("empleado: "+empleado.getNombre()+" hoarario: "+empleado.getHorario().getDescripcion()+" id_empresa:"+id_empresa);
            }

            @Override
            public void onFailure(Call<Empleado> call, Throwable t) {
                Toast.makeText(null,"No se pudo recuperar el empleado",Toast.LENGTH_LONG).show();
                tvM.setText("No hay respuesta para la peticion");
            }
        });
    }

    private void realizarTareaObtenerGeocerca(){
        EmpleadoServicio empleadoServicio= API.obtenerApi().create(EmpleadoServicio.class);
        Call<Empleado> empleadoCall=empleadoServicio.obtener_empleado("cf5433728aad791a");
        empleadoCall.enqueue(new Callback<Empleado>() {
            @Override
            public void onResponse(Call<Empleado> call, Response<Empleado> response) {
                Empleado empleado=response.body();
                tvM.setText("empleado: "+empleado.getNombre());
            }

            @Override
            public void onFailure(Call<Empleado> call, Throwable t) {
                Toast.makeText(null,"No se pudo recuperar el empleado",Toast.LENGTH_LONG).show();
                tvM.setText("No hay respuesta para la peticion");
            }
        });
    }

}
