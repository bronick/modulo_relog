package mx.com.visorus.http_db_json.utilidades;

import android.widget.TextView;
import android.widget.Toast;

import mx.com.visorus.http_db_json.modelos.Empleado;
import mx.com.visorus.http_db_json.tareas_de_red.API;
import mx.com.visorus.http_db_json.tareas_de_red.serviciosAPI.EmpleadoServicio;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by pc on 11/07/2018.
 */

public class UtilRetrofit {
    private static boolean peticion=false;

    private static boolean realizarTareaObtenerEmpleado(){
        EmpleadoServicio empleadoServicio= API.obtenerApi().create(EmpleadoServicio.class);
        Call<Empleado> empleadoCall=empleadoServicio.obtener_empleado("cf5433728aad791a");
        empleadoCall.enqueue(new Callback<Empleado>() {
            @Override
            public void onResponse(Call<Empleado> call, Response<Empleado> response) {
                Empleado empleado=response.body();
                peticion=true;
            }

            @Override
            public void onFailure(Call<Empleado> call, Throwable t) {
                Toast.makeText(null,"No se pudo recuperar el empleado",Toast.LENGTH_LONG).show();
                peticion=false;
            }
        });
        return peticion;
    }
}
