package mx.com.visorus.http_db_json.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by pc on 11/07/2018.
 */

public class AyudanteSQLite extends SQLiteOpenHelper {
    private static final String NOMBRE_BD="reloj_checador";

    private static final String TABLA_EMPRESA="" +
            "CREATE TABLE " +
            "empresa(" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "id_empresa INTEGER," +
            "clave TEXT," +
            "razon_social TEXT," +
            "rfc TEXT" +
            ")";
    private static final String TABLA_HORARIOS="" +
            "CREATE TABLE " +
            "horarios(" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "DT_RowId INTEGER," +
            "id_horario INTEGER," +
            "descripcion TEXT," +
            "entrada TEXT," +
            "salida TEXT," +
            "maxSalida TEXT," +
            "tipo INTEGER," +
            "minRetraso TEXT," +
            "retrasos INTEGER," +
            "premios INTEGER," +
            "activo INTEGER" +
            ")";
    private static final String TABLA_GEOCERCAS="" +
            "CREATE TABLE " +
            "geocercas(" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "DT_RowId INTEGER," +
            "id_geocerca INTEGER," +
            "descripcion TEXT," +
            "latitud TEXT," +
            "longitud TEXT," +
            "radio TEXT," +
            "wifimac TEXT," +
            "activo INTEGER" +
            ")";

    private static final String TABLA_LOGS="" +
            "CREATE TABLE " +
            "sistema_logs(" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "id_log INTEGER," +
            "id_empleado INTEGER," +
            "id_geocerca INTEGER," +
            "imagen TEXT," +
            "latitud TEXT," +
            "longitud TEXT," +
            "registro TEXT," +
            "activo INTEGER," +
            "tipo INTEGER" +
            ")";

    private static final String TABLA_EMPLEADOS="" +
            "CREATE TABLE " +
            "empleados(" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "DT_RowId INTEGER," +
            "id_empleado INTEGER," +
            "clave TEXT," +
            "nombre TEXT," +
            "fk_id_empresa INTEGER," +
            "uuid TEXT," +
            "fk_id_horario INTEGER," +
            "status INTEGER," +
            "tipo INTEGER," +
            "FOREIGN KEY(fk_id_empresa) REFERENCES empresa(id_empresa)," +
            "FOREIGN KEY(fk_id_horario) REFERENCES horario(id_horario)" +
            ")";

    public AyudanteSQLite(Context context) {
        super(context, NOMBRE_BD, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
     sqLiteDatabase.execSQL(TABLA_EMPRESA);
     sqLiteDatabase.execSQL(TABLA_HORARIOS);
     sqLiteDatabase.execSQL(TABLA_GEOCERCAS);
     sqLiteDatabase.execSQL(TABLA_EMPLEADOS);
     sqLiteDatabase.execSQL(TABLA_LOGS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS empresa");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS horarios");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS geocercas");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS empleados");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS sistema_logs");
    }
}
