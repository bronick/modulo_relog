package mx.com.visorus.http_db_json.modelos;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pc on 06/07/2018.
 */

public class SistemaLog {
    private long id;
    private long id_empleado;
    private Geocerca geocerca;
    private String imagen;
    private String latitud;
    private String longitud;
    private String registro;
    private boolean status;
    private int tipo;

    public SistemaLog() {
    }

    public SistemaLog(long id, long id_empleado, Geocerca geocerca, String imagen, String latitud, String longitud, String registro, boolean status, int tipo) {
        this.id = id;
        this.id_empleado = id_empleado;
        this.geocerca = geocerca;
        this.imagen = imagen;
        this.latitud = latitud;
        this.longitud = longitud;
        this.registro = registro;
        this.status = status;
        this.tipo = tipo;
    }

    public String toJSON(){
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("id",getId());
            jsonObject.put("version",0);
            jsonObject.put("empleado_id",getId_empleado());
            jsonObject.put("registro",getRegistro());
            jsonObject.put("tipo",getTipo());
            jsonObject.put("geocerca_id",getGeocerca().getId());
            jsonObject.put("imagen", null);
            jsonObject.put("latitud", getLatitud());
            jsonObject.put("longitud",getLongitud());
            jsonObject.put("status",isStatus());
            return jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public Geocerca getGeocerca() {
        return geocerca;
    }

    public void setGeocerca(Geocerca geocerca) {
        this.geocerca = geocerca;
    }

    public long getId_empleado() {
        return id_empleado;
    }

    public void setId_empleado(long id_empleado) {
        this.id_empleado = id_empleado;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getRegistro() {
        return registro;
    }

    public void setRegistro(String registro) {
        this.registro = registro;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }
}
