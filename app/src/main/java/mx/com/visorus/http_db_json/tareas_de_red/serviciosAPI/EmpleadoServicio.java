package mx.com.visorus.http_db_json.tareas_de_red.serviciosAPI;

import mx.com.visorus.http_db_json.modelos.Empleado;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface EmpleadoServicio {
    @GET("vNomina/empleado/obtieneEmpleado")
    Call<Empleado> obtener_empleado(@Query("uuid") String uuid);
}
