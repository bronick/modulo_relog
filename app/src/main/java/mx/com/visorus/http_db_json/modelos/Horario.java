package mx.com.visorus.http_db_json.modelos;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by pc on 06/07/2018.
 */

public class Horario{
    private long DT_RowId;
    private long id;
    private String descripcion;
    private String entrada;
    private String salida;
    private String maxSalida;
    private int tipo;
    private String minRetraso;
    private int retrasos;
    private int premios;
    private boolean activo;


    public Horario() {
    }

    public Horario( long DT_RowId,long id, String descripcion, String entrada, String salida, String maxSalida, String minRetraso, boolean activo, int tipo) {
        this.DT_RowId=DT_RowId;
        this.id=id;
        this.descripcion= descripcion;
        this.entrada = entrada;
        this.salida = salida;
        this.maxSalida = maxSalida;
        this.minRetraso = minRetraso;
        this.activo = activo;
        this.tipo = tipo;
    }

    public int getPremios() {
        return premios;
    }

    public void setPremios(int premios) {
        this.premios = premios;
    }

    public void setDT_RowId(long DT_RowId) {
        this.DT_RowId = DT_RowId;
    }

    public int getRetrasos() {
        return retrasos;
    }

    public void setRetrasos(int retrasos) {
        this.retrasos = retrasos;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public long getDT_RowId() {
        return DT_RowId;
    }

    public void setDT_RowId(int DT_RowId) {
        this.DT_RowId = DT_RowId;
    }


    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEntrada() {
        return entrada;
    }

    public void setEntrada(String entrada) {
        this.entrada = entrada;
    }

    public String getSalida() {
        return salida;
    }

    public void setSalida(String salida) {
        this.salida = salida;
    }

    public String getMaxSalida() {
        return maxSalida;
    }

    public void setMaxSalida(String maxSalida) {
        this.maxSalida = maxSalida;
    }

    public String getMinRetraso() {
        return minRetraso;
    }

    public void setMinRetraso(String minRetraso) {
        this.minRetraso = minRetraso;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public static Empresa parseJSON(String response){
        Gson gson=new GsonBuilder().create();
        Empresa empresa=gson.fromJson(response,Empresa.class);
        return empresa;
    }
}
